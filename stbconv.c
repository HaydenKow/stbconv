#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#define STBI_NO_PIC
#define STBI_NO_PNM
#define STBI_NO_PSD
#define STBI_NO_HDR
#define STBI_NO_LINEAR
#define STBI_MAX_DIMENSIONS (512)
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
// Unused currently
//#define STB_IMAGE_WRITE_IMPLEMENTATION
//#include "stb_image_write.h"
#include "argparse.h"
#include "smallargs.h"

/* Options */
//#define VERBOSE
static int no_mipmaps = 0;
static int twiddle = 0;
static int global_index = 0;
static int rgb565 = 0;

#ifdef ARG_SMALLARGS
static sarg_opt my_opts[] = {
    {"h", "help", "show help text", BOOL, NULL},
    {"nm", NULL, "no mipmaps", BOOL, NULL},
    {"gi", NULL, "global index number", INT, NULL},
    {"t", NULL, "twiddle", BOOL, NULL},
    {"5", NULL, "rgb565 color", BOOL, NULL},
    {NULL, NULL, NULL, INT, NULL}};
#endif

#ifdef ARG_ARGPARSE
static const char *const usages[] = {
    "stbconv [options]",
    NULL,
};

struct argparse_option options[] = {
    OPT_HELP(),
    OPT_GROUP("Basic options"),
    OPT_BOOLEAN("nm", NULL, &no_mipmaps, "no mipmaps", NULL, 0, 0),
    OPT_BOOLEAN("t", NULL, &twiddle, "twiddle", NULL, 0, 0),
    OPT_BOOLEAN("5", NULL, &rgb565, "rgb565", NULL, 0, 0),
    OPT_INTEGER("gi", NULL, &global_index, "global index number", NULL, 0, 0),
    OPT_END(),
};
#endif

#if !defined(ARG_SMALLARGS) && !defined(ARG_ARGPARSE)
#error "No argument parser defined"
#endif

/* PVR Format */
typedef struct gbix_header
{
  // char magic[4]; /* GBIX */
  uint32_t magic; /* GBIX */
  int32_t len;    /* Always 8 */
  int32_t global_index;
  // char padding[4]; /* 0x20 0x20 0x20 0x20 */
  uint32_t padding; /* 0x20 0x20 0x20 0x20 */
} gbix_header;

typedef struct pvr_header
{
  // char magic[4];      /* PVRT */
  uint32_t magic;    /* PVRT */
  int32_t len;       /* Length of file - these 4 bytes - this offset (0x14) */
  uint8_t pixel_fmt; /* Described below */
  uint8_t data_fmt;  /* Described below */
  uint16_t padding;  /* 0x00 0x00 */
  uint16_t width;
  uint16_t height;
} pvr_header;

#define PVR_HEADER_REMAINING_BYTES (0x08)
/* PVR pixel formats */
#define PVR_PIXEL_FMT_ARGB_1555 (0x00) /* Not supported */
#define PVR_PIXEL_FMT_RGB_565 (0x01)   /* Supported */
#define PVR_PIXEL_FMT_ARGB_4444 (0x02) /* Not supported */
#define PVR_PIXEL_FMT_YUV_422 (0x03)   /* Not supported */
#define PVR_PIXEL_FMT_BUMP (0x04)      /* Not supported */
#define PVR_PIXEL_FMT_PAL_4BPP (0x05)  /* Not supported */
#define PVR_PIXEL_FMT_PAL_8BPP (0x06)  /* Not supported */

/* PVR data formats */
#define PVR_DATA_FMT_UNKNOWN_00 (0x00)           /* Not supported */
#define PVR_DATA_FMT_SQUARE_TWIDDLED (0x01)      /* Supported */
#define PVR_DATA_FMT_SQUARE_TWIDDLED_MIP (0x02)  /* Not supported */
#define PVR_DATA_FMT_VQ (0x03)                   /* Not supported */
#define PVR_DATA_FMT_VQ_MIP (0x04)               /* Not supported */
#define PVR_DATA_FMT_PAL_4BPP (0x05)             /* Not supported , Might be wrong */
#define PVR_DATA_FMT_PAL_4BPP_MIP (0x06)         /* Not supported , Might be wrong */
#define PVR_DATA_FMT_PAL_8BPP (0x07)             /* Not supported , Might be wrong */
#define PVR_DATA_FMT_PAL_8BPP_MIP (0x08)         /* Not supported , Might be wrong */
#define PVR_DATA_FMT_RECT (0x09)                 /* Not supported */
#define PVR_DATA_FMT_UNKNOWN_0A (0x0A)           /* Not supported */
#define PVR_DATA_FMT_RECT_STRIDE (0x0B)          /* Not supported */
#define PVR_DATA_FMT_UNKNOWN_A0 (0x0C)           /* Not supported */
#define PVR_DATA_FMT_RECT_TWIDDLED (0x0D)        /* Not supported */
#define PVR_DATA_FMT_UNKNOWN_0E (0x0E)           /* Not supported */
#define PVR_DATA_FMT_UNKNOWN_0F (0x0F)           /* Not supported */
#define PVR_DATA_FMT_SMALL_VQ (0x10)             /* Not supported */
#define PVR_DATA_FMT_SMALL_VQ_MIP (0x11)         /* Not supported */
#define PVR_DATA_FMT_SQUARE_TWIDDLED_MIP2 (0x12) /* Not supported */

/* Twiddling funcs */
/* Linear/iterative twiddling algorithm from Marcus' tatest */
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#define TWIDTAB(x) ((x & 1) | ((x & 2) << 1) | ((x & 4) << 2) | ((x & 8) << 3) | ((x & 16) << 4) | \
                    ((x & 32) << 5) | ((x & 64) << 6) | ((x & 128) << 7) | ((x & 256) << 8) | ((x & 512) << 9))
#define TWIDOUT(x, y) (TWIDTAB((y)) | (TWIDTAB((x)) << 1))

static void twiddle_8bpp(void *src, void *dst, uint32_t w, uint32_t h)
{
  uint32_t x, y, yout, min, mask;

  min = MIN(w, h);
  mask = min - 1;

  uint8_t *pixels;
  uint16_t *vtex;
  pixels = (uint8_t *)src;
  vtex = (uint16_t *)dst;

  for (y = 0; y < h; y += 2)
  {
    yout = y;
    for (x = 0; x < w; x++)
    {
      vtex[TWIDOUT((yout & mask) / 2, x & mask) +
           (x / min + yout / min) * min * min / 2] =
          pixels[y * w + x] | (pixels[(y + 1) * w + x] << 8);
    }
  }
}

static void twiddle_16bpp(void *src, void *dst, uint32_t w, uint32_t h)
{
  uint32_t x, y, yout, min, mask;

  min = MIN(w, h);
  mask = min - 1;

  uint16_t *pixels;
  uint16_t *vtex;
  pixels = (uint16_t *)src;
  vtex = (uint16_t *)dst;

  for (y = 0; y < h; y++)
  {
    yout = y;

    for (x = 0; x < w; x++)
    {
      vtex[TWIDOUT(x & mask, yout & mask) +
           (x / min + yout / min) * min * min] = pixels[y * w + x];
    }
  }
}

int main(int argc, char **argv)
{

#ifdef ARG_SMALLARGS
#ifdef VERBOSE
  puts("small args");
  puts("------------------------------------------------------");
#endif
  sarg_root root;
  sarg_result *res;
  int i;

  int ret = sarg_init(&root, my_opts, "stbconv");
  assert(ret == SARG_ERR_SUCCESS);

  ret = sarg_parse(&root, (const char **)argv, argc);
  if (ret != SARG_ERR_SUCCESS)
  {
    printf("Parsing failed\n");
    sarg_help_print(&root);
    sarg_destroy(&root);
    return -1;
  }

  // arg parsing for options we know about
  ret = sarg_get(&root, "nm", &res);
  if (res->bool_val)
  {
    no_mipmaps = 1;
  }
  ret = sarg_get(&root, "gi", &res);
  if (res->int_val)
  {
    global_index = res->int_val;
  }
  ret = sarg_get(&root, "t", &res);
  if (res->bool_val)
  {
    twiddle = 1;
  }
  ret = sarg_get(&root, "5", &res);
  if (res->bool_val)
  {
    rgb565 = 1;
  }

  /* optional usage info */
  ret = sarg_get(&root, "help", &res);
  assert(ret == SARG_ERR_SUCCESS);

  if (argc == 1 || res->bool_val)
  {
    sarg_help_print(&root);
    sarg_destroy(&root);
    return 0;
  }

  // Configure argv, argc to point to the remaining arguments
  argc = sarg_get_extra(&root, (const char **)argv, argc);

  sarg_destroy(&root);
#endif

#ifdef ARG_ARGPARSE
#ifdef VERBOSE
  puts("argparse");
  puts("------------------------------------------------------");
  #endif

  struct argparse argparse;
  argparse_init(&argparse, options, usages, 0);
  argparse_describe(&argparse, "\nA brief description of what the program does and how it works.", "\nAdditional description of the program after the description of the arguments.");
  if (argc == 1)
  {
    argparse_usage(&argparse);
    return 0;
  }

  // Configure argv, argc to point to the remaining arguments
  argc = argparse_parse(&argparse, argc, (const char **)argv);
#endif

  /* Output options and arguments */
#ifdef VERBOSE
  if (no_mipmaps != 0)
    printf("no_mipmaps: %d\n", no_mipmaps);
  if (twiddle != 0)
    printf("twiddle: %d\n", twiddle);
  if (rgb565 != 0)
    printf("rgb565: %d\n", rgb565);
  if (global_index != 0)
    printf("global_index: %d\n", global_index);
#endif
  if (argc != 0)
  {
#ifdef VERBOSE
    printf("argc: %d\n", argc);
    int i;
    for (i = 0; i < argc; i++)
    {
      printf("argv[%d]: %s\n", i, *(argv + i));
    }
#endif
  }
  else
  {
    printf("Error: No input image given\n");
  }

  for (int i = 0; i < argc; i++)
  {
    int x, y, n;
    unsigned char *img = stbi_load(argv[i], &x, &y, &n, 3);
    if (img == NULL)
    {
      printf("Error: loading input image.\n");
      exit(1);
    }
    printf("Loaded '%s' with a width of %dpx, a height of %dpx and %d channels.\n", argv[i], x, y, n);

    // Convert the input image to rgb565
    size_t img_size = x * y * n;
    int bpp = 2;
    size_t rgb565_img_size = x * y * bpp;
    unsigned char *rgb565_img = malloc(img_size);
    if (rgb565_img == NULL)
    {
      printf("Error: Unable to allocate memory.\n");
      exit(1);
    }
    if (rgb565)
    {
      for (int img_i = 0, rgb565_img_i = 0; img_i < img_size; img_i += n, rgb565_img_i += bpp)
      {
        uint8_t r = img[img_i + 0];
        uint8_t g = img[img_i + 1];
        uint8_t b = img[img_i + 2];
        uint16_t rgb565 = ((r >> 3) << 11) | ((g >> 2) << 5) | (b >> 3);
        rgb565_img[rgb565_img_i + 1] = (rgb565 >> 8) & 0xFF;
        rgb565_img[rgb565_img_i + 0] = rgb565 & 0xFF;
      }
    }

    /* Now Twiddle the image */
    if (twiddle)
    {
      unsigned char *twid_img = malloc(img_size);
      twiddle_16bpp(rgb565_img, twid_img, x, y);
      free(rgb565_img);
      rgb565_img = twid_img;
    }

    /* Fill out structures */
    gbix_header gbix;
    gbix.magic = 0x58494247;
    gbix.global_index = global_index;
    gbix.len = 8;
    gbix.padding = 0x20202020;

    pvr_header pvr;
    pvr.magic = 0x54525650;
    pvr.len = x * y * /* pvr_bpp */ 2 + PVR_HEADER_REMAINING_BYTES;
    pvr.pixel_fmt = PVR_PIXEL_FMT_RGB_565;
    pvr.data_fmt = PVR_DATA_FMT_SQUARE_TWIDDLED;
    // pvr.data_fmt = PVR_DATA_FMT_RECT;
    pvr.padding = 0x0000;
    pvr.width = x;
    pvr.height = y;

    /* Write raw rgb565 data */
    /*
    FILE *out = fopen("texture.raw", "wb");
    if (out == NULL)
    {
      printf("Error: Unable to open output file.\n");
      exit(1);
    }
    fwrite(rgb565_img, rgb565_img_size, 1, out);
    fclose(out);
    printf("Wrote Raw file.\n");
    */

    /* Write out BMP file */
    // stbi_write_bmp("texture.bmp", x,y, n, img);

    /* Write the PVR file */
    char *ext = strrchr(argv[i], '.') + 1;
    memcpy(ext, "pvr", 4);
    FILE *out = fopen(argv[i], "wb");
    if (out == NULL)
    {
      printf("Error: Unable to open output file.\n");
      exit(1);
    }
    fwrite(&gbix, sizeof(gbix), 1, out);
    fwrite(&pvr, sizeof(pvr), 1, out);
    fwrite(rgb565_img, rgb565_img_size, 1, out);
    fclose(out);
    printf("Wrote %s file.\n", argv[i]);

    stbi_image_free(img);
    free(rgb565_img);
  }

  return 0;
}
