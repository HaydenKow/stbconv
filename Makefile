#docker run --rm -ti -v `pwd`:/mnt mmozeiko/mingw-w64 make win
CC ?= gcc
INCS := -I.
DEFINES ?= -DSTANDALONE_BINARY=1
CFLAGS ?= -O2 -s $(INCS) $(DEFINES)
OPT_HEADERS ?=

all: stbconv_argparse stbconv_smallargs

win: CC = x86_64-w64-mingw32-gcc
win: stbconv_argparse stbconv_smallargs

stbconv_smallargs: DEFINES := $(DEFINES) -DARG_SMALLARGS
stbconv_smallargs: OPT_HEADERS := smallargs.h
stbconv_smallargs: stbconv_smallargs.o smallargs.h
	@echo "$(CC) stbconv_smallargs.o"
	@$(CC) $(CFLAGS) -o $@ stbconv_smallargs.o
	@echo "> $@"

stbconv_argparse: DEFINES := $(DEFINES) -DARG_ARGPARSE
stbconv_argparse: OPT_HEADERS := argparse.h
stbconv_argparse: stbconv_argparse.o argparse.o
	@echo "$(CC) sstbconv_argparse.o"
	@$(CC) $(CFLAGS) -o $@ stbconv_argparse.o argparse.o
	@echo "> $@"

stbconv_smallargs.o: stbconv.c stb_image.h $(OPT_HEADERS)
	@$(CC) $(CFLAGS) -c $< -o stbconv_smallargs.o

stbconv_argparse.o: stbconv.c stb_image.h $(OPT_HEADERS)
	@$(CC) $(CFLAGS) -c $< -o stbconv_argparse.o

argparse.o: argparse.c argparse.h

.PHONY: clean
clean:
	-rm -f stbconv.o argparse.o

.PHONY: distclean
distclean: clean
	-rm -f stbconv*.o stbconv_smallargs stbconv_argparse

.PHONY: all
.PHONY: win